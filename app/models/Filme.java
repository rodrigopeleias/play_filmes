package models;

import java.util.*;

import javax.persistence.*;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name = "filmes_cult")
public class Filme extends Model {

	@Id
	public Long id;

	@Constraints.Required
	public String nome;

	public String tipo;

	public Double nota;

	public Integer duracao;

	public Integer ano;

	public String genero;

	public Integer votos;

	public String url;

	@ManyToOne
	public Diretor diretor;

	public static Model.Finder<Long, Filme> find = new Model.Finder<Long, Filme>(Long.class,Filme.class);

	@Override
	public String toString() {
		return nome;
	}


}