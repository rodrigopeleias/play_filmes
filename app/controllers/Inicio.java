package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class Inicio extends Controller {

    public static Result index() {
        return ok(views.html.inicio.render());
    }   
}
