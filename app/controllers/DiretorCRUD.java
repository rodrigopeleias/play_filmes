package controllers;

import static play.data.Form.form;

import java.util.List;

import models.Diretor;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;

public class DiretorCRUD extends Controller {

    public static final Form diretorForm = Form.form(Diretor.class);

    @SecureSocial.UserAwareAction
    public static Result lista() {
        List<Diretor> diretores = Diretor.find.findList();
        Identity user = (Identity)ctx().args.get(SecureSocial.USER_KEY);
        final String userName = user != null ? user.fullName() : "guest";
        return ok(views.html.diretor.render(diretores, userName));
    }

    @SecureSocial.SecuredAction
    public static Result novoDiretor() {
    	return ok(views.html.novoDiretor.render(diretorForm));
    }

    @SecureSocial.SecuredAction
    public static Result gravar() {
    	// a variável form retorna as informações preenchidas sobre o diretor
    	Form<Diretor> form = diretorForm.bindFromRequest();
    	if(form.hasErrors()) {
    		flash("erro", "Foram identificados problemas no cadastro");
    		return ok(views.html.novoDiretor.render(diretorForm));
    	}
    	Diretor diretor = form.get();
    	diretor.save();
    	flash("sucesso", "Registro gravado com sucesso");
    	return redirect(routes.DiretorCRUD.lista());
    }

    public static Result detalhar(Long id) {
        Form<Diretor> dirForm = form(Diretor.class).fill(Diretor.find.byId(id));
        return ok(views.html.alterarDiretor.render(id, dirForm));
    }

    @SecureSocial.SecuredAction
    public static Result alterar(Long id) {
        form(Diretor.class).fill(Diretor.find.byId(id));
        Form<Diretor> alterarForm = form(Diretor.class).bindFromRequest();
        if (alterarForm.hasErrors()) {
            return badRequest(views.html.alterarDiretor.render(id, alterarForm));
        }
        alterarForm.get().update(id);
        flash("sucesso", "Diretor " + alterarForm.get().nome + " alterado com sucesso!");
        return redirect(routes.DiretorCRUD.lista());
    }

    @SecureSocial.SecuredAction
    public static Result remover(Long id) {
        try {
        	Diretor.find.ref(id).delete();
        	flash("sucesso","Diretor removido com sucesso!");        	
        } catch (Exception e) {
        	flash("erro", play.i18n.Messages.get("global.erro"));
        }
        return lista();	
    }
    
}
